#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 17:10:41 2020

@author: heepark
"""

import numpy as np
import utility as u
import test_functions as tf
import nonlinear_solvers as ns
import test_engine as te

branin = tf.Branin()

# Solver options
# inf_rel_tol=1.e-3, inf_abs_tol=1.e-5,
# res_rel_tol=1.e-5, res_abs_tol=1.e-5,
# derivative_eps=5.e-6, maxit=50
trc = ns.TR(); trc.res_rel_tol=1.e-2; trc.inf_abs_tol=1.e-2;
trc.method = 1; trc.name='TRC'; # Cauchy point 1

ntrdc = ns.TR2(); ntrdc.res_rel_tol=1.e-2; ntrdc.inf_abs_tol=1.e-2;
ntrdc.name='NTRDC';  # Dogleg

ntr = ns.TR3(); ntr.res_rel_tol=1.e-2; ntr.inf_abs_tol=1.e-2;
ntr.name='NTR'; # Cauchy point 1

lm = ns.LM()

#x0 = np.array([[6.,12.]])
x0 = np.array([[ 4. , 12.]])

methods = [lm, trc, ntrdc, ntr]

Branin_test = te.TestEngine(function=branin,
                            methods=methods,
                            x0s=x0, contour_plot=0, 
                            plot_list=[])

Branin_test.evaluate()
Branin_test.print_report()

#run1 = Branin_test.info['LM'][0]
run2 = Branin_test.info['TRC'][0]
run3 = Branin_test.info['NTRDC'][0]
run4 = Branin_test.info['NTR'][0]

branin.domain = np.array(([-5.,10.],[-4.,16.]))
branin.contour_mesh(25)

x = branin.contour_x 
y = branin.contour_y
z = branin.contour_z

#u.plot_contour_tr(x,y,z,x0[0],branin.mins,
#                  run1[0],run1[-1],
#                  'PTR iter:%i(%i)'%(run1[2],run1[4]),
#                  title='PTR',show=True)
u.plot_contour_tr(x,y,z,x0[0],branin.mins,
                  run2[0],run2[-1],
                  'TRC iter:%i(%i)'%(run2[2],run2[4]),
                  title='TRC',show=True)
u.plot_contour_tr(x,y,z,x0[0],branin.mins,
                  run3[0],run3[-1],
                  'NTRDC iter:%i(%i)'%(run3[2],run3[4]),
                  title='NTRDC',show=True)
u.plot_contour_tr(x,y,z,x0[0],branin.mins,
                  run4[0],run4[-1],
                  'NTR iter:%i(%i)'%(run4[2],run4[4]),
                  title='NTR',show=True)
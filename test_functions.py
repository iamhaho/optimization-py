#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 08:48:39 2020

@author: heepark
"""

import numpy as np


pi = np.pi

class TestFunction(object):
    def __init__(self):
        self.name = 'None'
        self.mins = np.ones(2)
        self.min_values = np.array([0.0])
        self.domain = np.array(([-2.048,2.048],[-2.048,2.048]))
        self.contour_x = np.zeros(0)
        self.contour_y = np.zeros(0)
        self.contour_z = np.zeros(0)
    
    def contour_mesh(self,n):
        '''
        n = number of points in each dimension (i.e. resolution of contour)
        '''
        xmin = self.domain[0,0]
        xmax = self.domain[0,1]
        ymin = self.domain[1,0]
        ymax = self.domain[1,1]
        xs = np.linspace(xmin,xmax,n)
        ys = np.linspace(ymin,ymax,n)
        self.contour_x = xs
        self.contour_y = ys
        xv, yv = np.meshgrid(xs,ys)
        contour_z = self.f(np.array([xv,yv]))
        self.contour_z = contour_z
        return contour_z
    
    def f(self,x):
        return x
        

class Branin(TestFunction):
    '''
    2-dimensional Branin function
    ref: https://www.sfu.ca/~ssurjano/branin.html
    '''
    def __init__(self):
        super().__init__()
        self.name = 'Branin'
        self.a = 1.
        self.b = 5.1/(4.*pi**2.)
        self.c = 5./pi
        self.r = 6.
        self.s = 10.
        self.t = 1./(8.*pi)
        self.mins = np.array([[-pi,12.275],[pi,2.275],[9.42478,2.475]])
        self.min_values = np.array([0.397887,0.397887,0.397887])
        self.domain = np.array(([-4.,10.],[0.,15.]))

    def f(self,x):
        '''
        2-dimensional Branin function
        x: np.array([x0,x1])
        '''
        z = self.a*(x[1] - self.b*x[0]**2. +  self.c*x[0] - self.r)**2. \
        + self.s*(1.-self.t)*np.cos(x[0]) + self.s
        return z


class Matyas(TestFunction):
    '''
    2-dimensional Matyas function
    ref: https://en.wikipedia.org/wiki/TestFunctions_for_optimization
    '''
    def __init__(self):
        super().__init__()
        self.name = 'Matyas'
        self.mins = np.array([[0.0,0.0]])
        self.min_values = np.array([0.0])
        self.domain = np.array(([-10.,10.],[-10.,10.]))
        
    def f(self,x):
        '''
        2-dimensional Matyas function
        x: np.array([x0,x1])
        '''
        z = 0.26*(x[0]**2 + x[1]**2)-0.48*x[0]*x[1]
        return z


class Booth(TestFunction):
    '''
    2-dimensional Booth function
    ref: https://www.sfu.ca/~ssurjano/booth.html
    '''
    def __init__(self):
        super().__init__()
        self.name = 'Booth'
        self.mins = np.array([[1.0,3.0]])
        self.min_values = np.array([0.0])
        self.domain = np.array(([-7.5,10.],[-7.5,10.]))
        
    def f(self,x):
        '''
        2-dimensional Booth function
        x: np.array([x0,x1])
        '''
        z = (x[0]+2.*x[1]-7.)**2. + (2.*x[0]+x[1]-5.)**2.
        return z


class Bohachevsky(TestFunction):
    '''
    2-dimensional Bohachevsky function
    https://www.sfu.ca/~ssurjano/boha.html
    '''
    def __init__(self):
        super().__init__()
        self.name = 'Bohachevsky'
        self.mins = np.array([[0.0,0.0]])
        self.min_values = np.array([0.0])
        self.domain = np.array(([-75.,75.],[-75.,75.]))
        
    def f(self,x):
        '''
        2-dimensional Bohachevsky function
        x: np.array([x0,x1])
        '''
        z = x[0]**2.+2*x[1]**2.-0.3*np.cos(3.*pi*x[0])- \
            0.4*np.cos(4.*pi*x[1]) + 0.7 
        return z


class Threehumpcamel(TestFunction):
    '''
    2-dimensional three hump camel function
    https://www.sfu.ca/~ssurjano/camel3.html
    '''
    def __init__(self):
        super().__init__()
        self.name = 'Three Hump Camel'
        self.mins = np.array([[0.0,0.0]])
        self.min_values = np.array([0.0])
        self.domain = np.array(([-2.2,2.2],[-3.,3.]))
    
    def f(self,x):
        '''
        2-dimensional three hump camel function
        x: np.array([x0,x1])
        '''
        z = 2.*x[0]**2.-1.05*x[0]**4.+x[0]**6./6.+x[0]*x[1]+x[1]**2.
        return z


class Goldstein(TestFunction):
    '''
    2-dimensional Goldstein-Price function
    https://www.sfu.ca/~ssurjano/goldpr.html
    '''
    def __init__(self):
        super().__init__()
        self.name = 'Goldstein-Price'
        self.mins = np.array([[0.0,-1.0]])
        self.min_values = np.array([3.0])
        self.domain = np.array(([-2.,1.],[-2.,1]))
    
    def f(self,x):
        '''
        2-dimensional Goldstein-Price function
        x: np.array([x0,x1])
        '''
        z = (1.+(x[0]+x[1]+1.)**2.*(19.-14.*x[0]+3.*x[0]**2.-14.*x[1]+ \
             6.*x[0]*x[1]+3*x[1]**2.)) * \
            (30.+(2*x[0]-3.*x[1])**2.*(18.-32.*x[0]+12.*x[0]**2.+48.*x[1]- \
             36*x[0]*x[1]+27.*x[1]**2.))
        return z


class Easom(TestFunction):
    '''
    2-dimensional Easom function
    https://www.sfu.ca/~ssurjano/goldpr.html
    '''
    def __init__(self):
        super().__init__()
        self.name = 'Easom'
        self.mins = np.array([[pi,pi]])
        self.min_values = np.array([-1.0])
        self.domain = np.array(([0.5*pi,1.5*pi],[0.5*pi,1.5*pi]))
    
    def f(self,x):
        '''
        2-dimensional Easom function
        x: np.array([x0,x1])
        '''
        z = -np.cos(x[0])*np.cos(x[1])* \
             np.exp(-1.*(x[0]-pi)**2.-1.*(x[1]-pi)**2.)
        return z


class Sphere(TestFunction):
    '''
    multi-dimensional sphere function
    https://www.sfu.ca/~ssurjano/spheref.html
    '''
    def __init__(self):
        super().__init__()
        self.name = 'Sphere'
        self.mins = np.array([[0.0,0.0]])
        self.min_values = np.array([0.0])
        self.domain = np.array(([-5.12,5.12],[-5.12,5.12]))

    def set_dimensions(self,x):
        n = len(x)
        self.mins = np.zeros(n)

    def f(self,x):
        '''
        multi-dimensional sphere function
        x: 1-d vector
        '''
        z = sum(x**2.)
        return z


class Rosenbrock(TestFunction):
    '''
    multi-dimensional Rosenbrock function
    https://www.sfu.ca/~ssurjano/rosen.html
    '''
    def __init__(self):
        super().__init__()
        self.name = 'Rosenbrock'
        self.mins = np.array([[1.0,1.0]])
        self.min_values = np.array([0.0])
        self.domain = np.array(([-2.048,2.048],[-2.048,2.048]))

    def set_dimensions(self,x):
        n = len(x)
        self.mins = np.ones(n)

    def f(self,x):
        '''
        multi-dimensional Rosenbrock function
        x: 1-d vector
        '''
        z = sum(100.0*(x[1:] - x[:-1]**2.0)**2.0 + (1 - x[:-1])**2.0)
        return z

class Levy(TestFunction):
    '''
    multi-dimensional Levy function
    https://www.sfu.ca/~ssurjano/levy.html
    '''
    def __init__(self):
        super().__init__()
        self.name = 'Levy'
        self.mins = np.array([[1.0,1.0]])
        self.min_values = np.array([0.0])
        self.domain = np.array(([-7.5,7.5],[-10.,10.]))
    
    def set_dimensions(self,x):
        n = len(x)
        self.mins = np.ones(n)
        
    def f(self,x):
        '''
        multi-dimensional Levy function
        x: 1-d vector
        '''
        w = 1. + (x-1.)/4.
        z = np.sin(pi*w[0])**2. + sum( (w[:-1]-1)**2. * \
            (1.+10.*np.sin(pi*w[:-1]+1.)**2.) ) + \
            (w[-1]-1.)**2.*(1.+np.sin(2*pi*w[-1])**2.)
        return z





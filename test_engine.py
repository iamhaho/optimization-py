#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 10:11:39 2020

@author: heepark
"""

from smt.sampling_methods import LHS
from time import time
import utility as u
import numpy as np
import warnings
import random
warnings.filterwarnings("error")

def sample_x0(domain, num_sample, savename=""):
    '''
    input domain = np.array([[x_min,x_max],[y_min,y_max]])
    '''
    samples = LHS(xlimits=domain)
    x = samples(num_sample)
    if savename != "":
        np.save(savename,x)
    
    return x


def print_summary(tests):
    
    print("Function: All")
    print("Method    cases  correct  outer  inner  avg.time   total")
    for i in range(len(tests[0].methods)):
        outer = 0
        inner = 0
        tot_time = 0.0
        tot_cases = 0
        correct = 0
        for k in range(len(tests)):
            name = tests[k].methods[i].name
            correct += sum(tests[k].correct[name])
            num_cases = len(tests[k].info[name])
            for j in range(num_cases):
                outer += tests[k].info[name][j][2]
                inner += tests[k].info[name][j][4]
                tot_time += tests[k].info[name][j][5]
                tot_cases += 1
        
        pct = correct / tot_cases * 100.
        outer = outer / tot_cases
        inner = inner / tot_cases
        time_per_case = tot_time / tot_cases                    
        
        print("%s  %3i  %6.3g%%  %5.3g  %5.3g  %7.2e   %7.2e" % 
              (name.ljust(10), tot_cases, pct, outer, inner, 
               time_per_case, tot_time))
    print(" ")


class TestEngine(object):
    '''
    Test a function with list of given methods
    
    INPUT
    function: TestFunction class
    methods: [] - list of solver methods from nonlinear_solver.py
    x0s: [] - list of initial guess 
    contour_plot: 0:No, 1:Yes, 2:Failed, 3:Selective, 4:Random
    plot_list: [] of strings of method names failures for contour_plot = 2
               [] of int of initial guess index for contour_plot = 3
               [int] a number of plots to randomly select
    
    OUTPUT
    each methods overall compute time, individual times, accuracy, contour 
    plots, etc
    '''
    def __init__(self, function=None, methods=[], x0s=[], contour_plot=0,
                 plot_list=[0],print_option=0):
        self.func = function
        self.methods = methods
        self.x0s = x0s
        self.info = dict()
        self.overall_times = dict()
        self.accuracy = dict()
        self.correct = dict()
        self.plot = contour_plot
        self.plot_resolution = 21 
        self.plot_list = plot_list
        self.sol_error_report = 0.01 # within 1 percent of the true solution
        self.print_option = print_option
        
    def evaluate(self):
        methods = self.methods
        x0s = self.x0s
        func = self.func
        f = self.func.f
        info = self.info 
        print_option = self.print_option
        plot = self.plot
        overall_times = self.overall_times
        accuracy = self.accuracy
        correct = self.correct
        print("Function: " + func.name)
        for i in range(len(methods)):
            name = methods[i].name
            print("  Method: " + name)
            info[name] = []
            overall_times[name] = []
            accuracy[name] = []
            correct[name] = []
            overall_time_start = time()
            for j in range(len(x0s)):
                info[name].append(methods[i].solve(x0s[j],f))
                if info[name][0]:
                    accuracy[name].append(info[name][-1][3] - 
                                          func.min_values[0])
                    if accuracy[name][-1] <= self.sol_error_report:
                        correct[name].append(True)
                    else:
                        correct[name].append(False)
                
            overall_times[name] = time() - overall_time_start
                
        for i in range(len(self.methods)):
            name = self.methods[i].name
            print("%s overall time: %.3g s" % (name, self.overall_times[name]))

        plot_list = self.plot_list
        if plot == 1:
            indices = np.arange(len(x0s))
        elif plot == 2:
            indices = np.zeros([len(x0s)],dtype='int')
            for method in plot_list:
                # gather all the correct ones
                indices += correct[method]
            # and then invert logic to get the failures
            indices = np.logical_not(indices)
            indices = np.arange(len(x0s))[indices]
        elif plot == 3:
            indices = plot_list
        elif plot == 4: 
            if plot_list[0] > len(x0s):
                indices = np.arange(len(x0s))
            else:
                indices = range(len(x0s))
                indices = random.sample(indices, plot_list[0])
        else: 
            indices = []

        if plot != 0:
            orig_domain = np.copy(func.domain)
            for i in indices:
                # for each initial points
                func.domain = np.copy(orig_domain)
                sols = []
                labels = []
                xmin = [] 
                xmax = []
                ymin = []
                ymax = []
                for j in range(len(methods)):
                    method = methods[j].name
                    # results of each method
                    sols.append(info[method][i][0])
                    labels.append("%s iter:%i(%i)" % 
                                  (method, info[method][i][2],
                                   info[method][i][4]))
                    # for contour plot
                    xmin.append(info[method][i][6][0])
                    xmax.append(info[method][i][6][1])
                    ymin.append(info[method][i][6][2])
                    ymax.append(info[method][i][6][3])
                
                # re-adjust domain size if the steps were outside the 
                # original domain
                xmin = np.array(xmin)
                xmax = np.array(xmax)
                ymin = np.array(ymin)
                ymax = np.array(ymax)
                func.domain[0,0] = np.array([xmin.min(),
                                             func.domain[0,0]]).min()
                func.domain[0,1] = np.array([xmax.max(),
                                             func.domain[0,1]]).max()
                func.domain[1,0] = np.array([ymin.min(),
                                             func.domain[1,0]]).min()
                func.domain[1,1] = np.array([ymax.max(),
                                             func.domain[1,1]]).max()
                func.contour_mesh(self.plot_resolution)
                title = func.name + " at (" + \
                      ", ".join(map('{0:6.3g}'.format,list(self.x0s[i]))) + ")"
                u.plot_contour(func.contour_x, func.contour_y, func.contour_z,
                               init_loc=x0s[i], fin_loc=func.mins,
                               sols=sols, labels=labels, 
                               title=title, show=False)

        if print_option == 1:
            self.print_reports()
        elif print_option == 2:
            self.print_accuracy()
        elif print_option == 3:
            self.print_results()

        
    def print_results(self):
        
        if len(self.info) == 0:
            print('Please evaluate the function first.')
            return
        
        for i in range(len(self.methods)):
            name = self.methods[i].name
            for j in range(len(self.x0s)):
                print(self.func.name, " at (", 
                      ", ".join(map('{0:6.3g}'.format,list(self.x0s[j]))),")")
                u.print_result(self.info[name][j],name)
                
        for i in range(len(self.methods)):
            name = self.methods[i].name
            print("%s overall time: %.3g s" % (name, self.overall_times[name]))
        print(" ")


    def print_summary(self):
        
        print("Function: %s" % self.func.name)
        print("Method      correct  outer  inner  avg.time   total")
        for i in range(len(self.methods)):
            name = self.methods[i].name
            pct = sum(self.correct[name])/len(self.correct[name])
            outer = 0
            inner = 0
            tot_time = 0.0
            num_cases = len(self.info[name])
            for j in range(num_cases):
                outer += self.info[name][j][2]
                inner += self.info[name][j][4]
                tot_time += self.info[name][j][5]
            
            outer = outer / num_cases
            inner = inner / num_cases
            time_per_case = tot_time / num_cases                     
            
            print("%s  %6.3g%%  %5.3g  %5.3g  %7.2e   %7.2e" % 
                  (name.ljust(10), pct*100., outer, inner, 
                   time_per_case, tot_time))
        print(" ")
            
        
    def print_accuracy(self):
        
        if len(self.info) == 0:
            print('Please evaluate the function first.')
            return
        
        for i in range(len(self.methods)):
            name = self.methods[i].name
            for j in range(len(self.x0s)):
                result = [self.accuracy[name][j], self.correct[name][j]]
                u.print_accuracy(self.info[name][j],result,name)
                
        for i in range(len(self.methods)):
            name = self.methods[i].name
            print("%s overall time: %.3g s" % (name, self.overall_times[name]))
        print(" ")
    
    
    def print_report(self):
        
        if len(self.info) == 0:
            print('Please evaluate the function first.')
            return
        
        for i in range(len(self.methods)):
            name = self.methods[i].name
            print("%s" % name)
            correct_percentage = sum(self.correct[name])/ \
                                 len(self.correct[name])
            print("Correct Percentage: %6.3g %%" % (correct_percentage*100.))                                 
            
            np_accuracy = np.array(self.accuracy[name])
            np_correct = np.array(self.correct[name])
            np_incorrect = np.logical_not(np.array(self.correct[name]))

            try:
                average_correct_difference = np_accuracy[np_correct].mean()
            except RuntimeWarning:
                average_correct_difference = np.nan
                pass
            
            if np.isnan(average_correct_difference):
                print("Average Difference from True solution when correct:"
                      " None")
            else:
                print("Average Difference from True solution when correct:"
                      " %11.4e" 
                      % average_correct_difference)

            try:
                average_incorrect_difference = np_accuracy[np_incorrect].mean()
            except RuntimeWarning:
                average_incorrect_difference = np.nan
                pass
            
            if np.isnan(average_incorrect_difference):
                print("Average Difference from True solution when incorrect:"
                      " None")
            else:
                print("Average Difference from True solution when incorrect:"
                      " %11.4e" 
                      % average_incorrect_difference)

            outer = 0
            inner = 0
            time_per_case = 0.0
            num_cases = len(self.info[name])
            for j in range(num_cases):
                outer += self.info[name][j][2]
                inner += self.info[name][j][4]
                time_per_case += self.info[name][j][5]
            
            outer = outer / num_cases
            inner = inner / num_cases
            time_per_case = time_per_case / num_cases
            
            print("Average Outer Iterations: %6.3g" % outer)
            print("Average Inner Iterations: %6.3g" % inner)
            print("Average Calculation Time: %6.3g" % time_per_case)
            print(" ")

                

            
                

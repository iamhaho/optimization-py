#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 08:59:39 2020

@author: heepark
"""

import matplotlib.pyplot as plt
import numpy as np

def plot_contour(x1,x2,z,init_loc=[],fin_loc=[],sols=[],labels=[],
                 title='Title',img_path='',img_name='',show=False,colorbar=True,ticks=True):
    '''
    Inputs:
        x1: np.array([]) - 1D contour_x-array
        x2: np.array([]) - 1D contour_y-array
        z: np.array([[],[]]) - 2D contour_z-array
        sols: [()] - list of tuples outputted by the methods
        labels: ['string'] - list of strings, names of the methods
        title: string - title of the plot
        show: True/False
    '''
    
    colors=['magenta','orange','gold','yellow','red','green','cyan','lime',
            'fuchsia','aqua']
#    lines=['None','None','None','None','None','None','None','None','None','None']
    lines=['-','-.','-','-.','-.','-','-.','--','-']
    markers=['o','^','*','x','*','x','o','^']
    bigmarkersizes = [8,8,16,16,16,16,8,8]
    edges = [1,1,2,2,2,2,1,1,1,1]
    markersizes = [6,6,10,10,10,10,6,6,4,4]
    fig = plt.figure(0,figsize=(12,12))
    ax = fig.subplots()
    zmin = z.min()
    zmax = z.max()
    levels = np.arange(zmin,zmax,((zmax-zmin)/(2*len(z))))
    x1min = x1.min()
    x1max = x1.max()
    x2min = x2.min()
    x2max = x2.max()
    im = ax.imshow(z, interpolation='bilinear', origin='lower', cmap='jet',
                   extent=(x1min,x1max,x2min,x2max))    
    CS = ax.contour(x1, x2, z, levels, linewidths=0.5, linestyles=['solid'])
    # make a colorbar for the contour lines
    if colorbar:
        CB = fig.colorbar(im, extend='both')
    if init_loc.any():
        ax.plot(init_loc[0],init_loc[1],color='white',marker='+',
                mew=5,ms=30,label='Initial Location')
    if fin_loc.any():
        for i in range(len(fin_loc)):
            final_loc = fin_loc[i]
            if i == 0:
                ax.plot(final_loc[0],final_loc[1],color='white',marker='x',
                        mew=5,ms=25,label='Minimum')
            else:
                ax.plot(final_loc[0],final_loc[1],color='white',marker='x',
                        mew=5,ms=25)
    if sols:
        for i in range(len(sols)):
            y = np.array(sols[i])
#            ax.plot(y[:,0],y[:,1],color=colors[i],marker=markers[i],mew=edges[i],
#                    linestyle=lines[i],ms=markersizes[i],lw=1.8,label=labels[i])
#            ax.plot(y[-1,0],y[-1,1],color=colors[i],marker=markers[i],
#                    linestyle=lines[i],mew=5,ms=bigmarkersizes[i],lw=0.5)
    if labels:
        leg = plt.legend(frameon=True, loc='best', fontsize=14)
        frame = leg.get_frame()
        frame.set_facecolor('lightgrey')
        frame.set_edgecolor('black')
        frame.set_alpha(0.8)
    if ticks:
        plt.tick_params(axis='x', labelsize=18)
        plt.tick_params(axis='y', labelsize=18)
    else:
        plt.tick_params(axis='x', which='both', bottom=False, top=False,       
                        labelbottom=False) 
        plt.tick_params(axis='y', which='both', bottom=False, top=False,       
                        labelleft=False) 
    ax.set_title(title, fontsize=20)
    if img_path != '' and img_name != '':
        plt.savefig(img_path + '/' + img_name, bbox_inches='tight', pad_inches=0.1, dpi=300)
    #plt.savefig(title+'_'+str(y[0,0])+'_'+str(y[0,1])+'.png')
    plt.savefig(title+'.png')
    plt.show()

def plot_contour_tr(x1,x2,z,init_loc=[],fin_loc=[],sols=[],deltas=[],
                    name=[],title='Title',show=False):
    fig = plt.figure(1,figsize=(12,12))
    ax = fig.subplots()
    zmin = z.min()
    zmax = z.max()
    levels = np.arange(zmin,zmax,((zmax-zmin)/(2*len(z))))
    x1min = x1.min()
    x1max = x1.max()
    x2min = x2.min()
    x2max = x2.max()
    im = ax.imshow(z, interpolation='bilinear', origin='lower', cmap='jet',
                   extent=(x1min,x1max,x2min,x2max))    
    CS = ax.contour(x1, x2, z, levels)
    # make a colorbar for the contour lines
    CB = fig.colorbar(im, extend='both')
    ax.plot(init_loc[0],init_loc[1],color='white',marker='+',lw=0,
           mew=5,ms=20,label='Initial Location')
    for i in range(len(fin_loc)):
        final_loc = fin_loc[i]
        if i == 0:
            ax.plot(final_loc[0],final_loc[1],color='white',marker='x',
                    lw=0,mew=5,ms=17,label='Minimum')
        else:
            ax.plot(final_loc[0],final_loc[1],color='white',marker='x',
                    lw=0,mew=5,ms=17)    
    y = np.array(sols)
    ax.plot(y[:,0],y[:,1],color='r',marker='o',
            ms=4,lw=1.2,label=name)
    
    shift = (x1max-x1min)/50.

    for i in range(min(7,len(deltas))):
        ax.add_artist(plt.Circle((y[i,0], y[i,1]), radius=deltas[i], 
                                 lw=2, color='gray', alpha=0.3))
        if i%4 == 0:
            ax.text(y[i,0]-deltas[i]+shift, y[i,1],i, fontsize=16,
                    alpha=0.6, color='black')
            ax.text(y[i,0]+shift, y[i,1]+shift,i, fontsize=16, color='red')
        elif i%4 == 1:
            ax.text(y[i,0]-deltas[i]-shift, y[i,1],i, fontsize=16,
                    alpha=0.6, color='black')
            ax.text(y[i,0]+shift, y[i,1]-shift,i, fontsize=16, color='red')
        elif i%4 == 2:
            ax.text(y[i,0]-deltas[i]+shift, y[i,1],i, fontsize=16,
                    alpha=0.6, color='black')
            ax.text(y[i,0]-shift, y[i,1]-shift,i, fontsize=16, color='red')
        else:
            ax.text(y[i,0]-deltas[i]-shift, y[i,1],i, fontsize=16,
                    alpha=0.6, color='black')
            ax.text(y[i,0]-shift, y[i,1]+shift,i, fontsize=16, color='red')
    plt.legend(frameon=True, loc='lower center', fontsize=18)
    plt.tick_params(axis='x', labelsize=18)
    plt.tick_params(axis='y', labelsize=18)
    ax.set_title(title, fontsize=20)
    plt.savefig(title+'_'+str(y[0,0])+'_'+str(y[0,1])+'_tr.png')
    plt.show()

def print_result(info,name):
    print('%s iter: %i(%i), inf_rel_err: %11.4e, inf_abs_err: %11.4e' % 
          (name,info[2],info[4],info[1][0],info[1][1]))
    print('%s iter: %i(%i), res_rel_err: %11.4e, res_abs_err: %11.4e' % 
          (name,info[2],info[4],info[1][2],info[1][3]))
    print('%s solution: ' % (name), end="")
    print('(',', '.join(map('{0:6.3g}'.format, info[0][-1])),')','=',
          '{0:6.3g}'.format(info[3]))
    print('%s time: %.3g s' % (name,info[5]))

def print_accuracy(info,result,name):
    print('%s iter: %i(%i), correct: %s by %11.4e' % 
          (name,info[2],info[4],result[1],result[0]))
    
    

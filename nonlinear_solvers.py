#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 13:43:58 2019

@author: heepark

nonlinear solvers engine (definitely not optimized, but maximized readability)
"""

import numpy as np
import scipy.linalg as sp
from time import time
from scipy.optimize import approx_fprime as fprime

INF_REL_ERR = 1
INF_ABS_ERR = 2
RES_REL_ERR = 3
RES_ABS_ERR = 4

def inf_norm_rel_update(x0,x1,tol=1.e-2):
    '''
    infinity norm relative update
    if the relative update on the largest change (infinity norm) is small 
    enough, it could be decalred as convergence.
    '''
    diff_sol = (x1-x0)/(x0+1.0e-30)
    inf_norm_rel_diff = max(abs(diff_sol))
    if inf_norm_rel_diff < tol:
        return True, inf_norm_rel_diff
    else:
        return False, inf_norm_rel_diff
    
def inf_norm_abs(x0,x1,tol=1.e-6):
    '''
    infinity norm absolute solution
    if the infinity norm change is less than the tolerance, it could be 
    declared as convergence.
    '''
    inf_norm_error = max(abs(x1-x0))
    if inf_norm_error <= tol:
        return True, inf_norm_error
    else:
        return False, inf_norm_error
    
def residual_abs(x0,x1,func,tol=1.e-5):
    '''
    residual absolute convergence criteria
    if the residual of the nonlinear solution is less than then tolerance,
    it could be declared as convergence.
    '''
    z1 = func(x1)
    if abs(z1) <= tol:
        return True, z1
    else:
        return False, z1
    
def residual_rel_update(x0,x1,func,tol=1.e-5):
    '''
    residual relative update
    if the update of the residual is relatively smaller than the tolerance, 
    it could be declared as convergence.
    '''
    z0 = func(x0)
    z1 = func(x1)
    res_rel_diff = (z1-z0)/z0
    if abs(res_rel_diff) <= tol:
        return True, res_rel_diff
    else:
        return False, res_rel_diff

def convergence(x0,x1,func,inf_rel_tol=1.e-4,inf_abs_tol=1.e-5,
                res_rel_tol=1.e-5,res_abs_tol=1.e-5):
    '''
    function to check all the necessary convergence criteria to declare
    convergence to the solver. It requires either absolute or relative 
    convergence of infinity norm and residual 
    '''
    converged = False
    inf_rel_converged, inf_rel_err = inf_norm_rel_update(x0,x1,inf_rel_tol)
    inf_abs_converged, inf_abs_err = inf_norm_abs(x0,x1,inf_abs_tol)
    res_rel_converged, res_rel_err = residual_rel_update(x0,x1,func,
                                                         res_rel_tol)
    res_abs_converged, res_abs_err = residual_abs(x0,x1,func,res_abs_tol)
        
    converged = ((inf_rel_converged or inf_abs_converged) and \
                (res_rel_converged or res_abs_converged))
    return (converged, inf_rel_err, inf_abs_err, res_rel_err, res_abs_err)

def Hessian(func,x,eps=1.e-6):
    '''
    Calculate the hessian matrix with finite differences (numerical derivative)
    Parameters:
       - func : callable function
       - x : ndarray
       - eps : relative perturbation 
    Returns:
       an array of shape (x.dim, x.ndim) + x.shape
       where the array[i, j, ...] corresponds to the second derivative x_ij
    '''
    N = x.shape[0]
    H = np.zeros((N,N)) 
    eps1 = eps*np.sign(x)    
    gd_0 = grad_f2(func,x,eps1)
    for i in range(N):
        xx0 = 1.*x[i]
        eps2 = eps*np.sign(xx0)*1.2
        x[i] = xx0 + eps2
        gd_1 =  grad_f2(func,x,eps2)
        H[:,i] = ((gd_1 - gd_0)/eps2).reshape(x.shape[0])
        x[i] =xx0
    return H  


def Hessian_old(func,x,eps=1.e-6):
    '''
    Another way to define Hessian.
    Good readability but inefficient, and fixed to 2x2 matrix.
    '''
    x1 = x[0]
    x2 = x[1]
    cross_deriv = (func(np.array([x1+eps,x2+eps]))
                   -func(np.array([x1+eps,x2-eps]))
                   -func(np.array([x1-eps,x2+eps]))
                   +func(np.array([x1-eps,x2-eps]))) / \
                   (4*eps*eps)
    
    H = np.zeros([len(x),len(x)])
    H[0,0] = (func(np.array([x1-eps,x2]))
              -2*func(x)
              +func(np.array([x1+eps,x2]))) \
              / (eps*eps)
    H[0,1] = cross_deriv
    H[1,0] = cross_deriv
    H[1,1] = (func(np.array([x1,x2-eps]))
              -2*func(x)
              +func(np.array([x1,x2+eps]))) \
              / (eps*eps)
    return H

def grad_f(func,x,eps=1.e-6):
    '''
    gradient of function (numerical derivative) for both signes
    calculating the steepest slope of the optimization test functions
    
    see: scipy fprime
    '''
    eps = eps*np.sign(x)
    gradf = fprime(x,func,eps)
    return gradf

def grad_f2(func,x,eps=1.e-6):
    '''
    gradient of function (numerical derivative) for positive only
    
    see: scipy fprime
    '''
    gradf = fprime(x,func,eps)
    return gradf

def grad_f_old(func,x,eps=1.e-6):
    '''
    Another way to define gradient.
    Good reability but inefficient.
    '''
    x1 = x[0]
    x2 = x[1]
    dfdx1_p = (func(np.array([x1+eps,x2])) - func(np.array([x1-eps,x2]))) / \
              (2*eps)
    dfdx2_p = (func(np.array([x1,x2+eps])) - func(np.array([x1,x2-eps]))) / \
              (2*eps)
    gradf = np.zeros([len(x)])
    gradf[0] = dfdx1_p 
    gradf[1] = dfdx2_p 
    return gradf


def cauchy_point(x0,gradf,H,delta,in_iter=False):
    '''
    Cauchy optimization method
    It calculates the steepest descent direction and step size
    '''
    gTBg = np.dot(np.dot(gradf.transpose(),H),gradf)
    gradf_norm = sp.norm(gradf)
    if gTBg <= 0.0:
        tau = 1.0
    else:
        tau = min([gradf_norm**3./(delta*gTBg),1.0])
    p = -tau*delta/gradf_norm*gradf
    return p
        
def cauchy_point2(x0,gradf,H,delta,in_iter=False):
    '''
      reference from Raphael Hausen's lecture notes (#6)
      url: http://www.numerical.rl.ac.uk/people/nimg/oupartc/ \
            lectures/raphael/lectures/lec6slides.pdf
    '''
    gTBg = np.dot(np.dot(gradf.transpose(),H),gradf)
    gradf_norm = sp.norm(gradf)
    if gTBg <= 0.0:
        auk = 1.0e9
    else:
        auk =  gradf_norm**2./gTBg
    ack = min(delta/gradf_norm, auk)
    p = -ack*gradf
    return p

def dogleg(x0,gradf,H,delta,in_iter=False):
    '''
      reference from Raphael Hausen's lecture notes (#7)
      url: http://www.numerical.rl.ac.uk/people/nimg/oupartc/ \
            lectures/raphael/lectures/lec7slides.pdf
    '''
    # del_yuk = yuk - x0
    gradf_norm = sp.norm(gradf)
    gTBg = np.dot(np.dot(gradf.transpose(),H),gradf)
    if gTBg <= 0.0:
        auk = 1.0e9
    else:
        auk =  gradf_norm**2./gTBg
    ack = min(delta/gradf_norm, auk)
    del_yuk = -ack*gradf    
    if sp.norm(del_yuk) >= delta:
        return del_yuk
    # del_yqnk = yqnk - x0
    del_yqnk = sp.solve(H,-gradf)
    if sp.norm(del_yqnk) <= delta:
        return del_yqnk
    else:
        A = del_yuk
        B = del_yqnk - del_yuk
        c0 = sp.norm(B)**2.
        c1 = 2.*np.dot(A,B)
        c2 = sp.norm(A)**2. - delta**2.
        tau_pos = (c1 + np.sqrt(c1**2. - 4.*c0*c2)) / (2.*c0)
        tau_neg = (c1 - np.sqrt(c1**2. - 4.*c0*c2)) / (2.*c0)
        tau = max([tau_pos, tau_neg])
        del_y = del_yuk + tau*(del_yqnk - del_yuk)

        yuk = x0 + del_yuk
        yqnk = x0 + del_yqnk
        A = yuk - x0
        B = yqnk - yuk
        c0 = sp.norm(B)**2.
        c1 = 2.*np.dot(A,B)
        c2 = sp.norm(A)**2. - delta**2.
        tau_pos = (c1**2. + np.sqrt(c1**2. - 4.*c0*c2)) / (2.*c0)
        tau_neg = (c1**2. - np.sqrt(c1**2. - 4.*c0*c2)) / (2.*c0)
        tau2 = max([tau_pos, tau_neg])
        del_y2 = yuk+tau2*(yqnk-yuk) - x0

#        if tau != tau2:
#            print('tau diff: %1.16e' % (tau-tau2))
            #print('del_y: %g != %g' % (del_y, del_y2))
        return del_y2

def dogleg2(x0,gradf,H,delta,in_iter=False):
    '''
      Not sure if this works correctly
    '''
    gTg = np.dot(gradf,gradf)
    gTBg = np.dot(np.dot(gradf.transpose(),H),gradf)
    tau = gTg/gTBg
    pu = -tau*gradf
    if (tau >= 0.0 and tau <= 1.0):
        p = tau*pu
    elif (tau > 1.0 and tau <= 2.0):
        pb = sp.solve(H,-gradf)
        p = pu + (tau-1)*(pb-pu)
    else:
        p = -delta*gradf/sp.norm(gradf)

    return p

class Solver(object):
    '''
    Default Solver parent object containing convergence criteria
    '''
    def __init__(self,inf_rel_tol=1.e-3, inf_abs_tol=1.e-5,
                 res_rel_tol=1.e-5, res_abs_tol=1.e-5,
                 derivative_eps=5.e-6, maxit=50):
        self.name = 'Solver'
        self.inf_rel_tol = inf_rel_tol
        self.inf_abs_tol = inf_abs_tol
        self.res_rel_tol = res_rel_tol 
        self.res_abs_tol = res_abs_tol
        self.derivative_eps = derivative_eps
        self.maxit = maxit
        self.func = None
        self.x0 = None
        
        
class SD(Solver):
    '''
    Solver child object
    Steepest Descent method
    '''
    def __init__(self,gamma=0.01):
        super().__init__()
        self.name = 'SD'
        self.gamma = gamma
        
    def solve(self,x0=None,func=None):
        '''
        Steepest Descent method solve function
        x0: initial guess
        func: a function to minimize 
        '''
        start_time = time()
        if self.func is not None:
            func = self.func
            if func is None:
                print('func is NOT defined')
                return
        if self.x0 is not None:
            x0 = self.x0
            if x0 is None:
                print('x0 is NOT defined')
                return
        maxit = self.maxit
        gamma = self.gamma
        eps = self.derivative_eps

        next_x = x0
        x_storage = []
        converged = []
        for i in range(maxit):
            current_x = next_x
            x_storage.append(current_x)
            #next_x = current_x - gamma * op.approx_fprime(current_x, func, 1.e-6) 
            next_x = current_x - gamma * grad_f(func,current_x,eps)
            converged = convergence(current_x,next_x,func,self.inf_rel_tol,
                                    self.inf_abs_tol,self.res_rel_tol,
                                    self.res_abs_tol)
            if converged[0]:
                x_storage.append(next_x)
                end_time = time() - start_time
                break
        z = func(x_storage[-1])
        end_time = time() - start_time
        x_storage = np.array(x_storage)
        xmin = x_storage[:,0].min()
        ymin = x_storage[:,1].min()
        xmax = x_storage[:,0].max()
        ymax = x_storage[:,1].max()
        return x_storage, converged[0:5], i+1, z, 0, end_time, \
               [xmin,xmax,ymin,ymax]


class NT(Solver):
    '''
    Solver child object
    Newton's method
    '''
    def __init__(self):
        super().__init__()
        self.name = 'NT'
            
    def solve(self,x0=None,func=None):
        '''
        Newton's method solve function
        x0: initial guess
        func: a function to minimize 
        '''
        start_time = time()
        if self.func is not None:
            func = self.func
            if func is None:
                print('func is NOT defined')
                return
        if self.x0 is not None:
            x0 = self.x0
            if x0 is None:
                print('x0 is NOT defined')
                return
        maxit = self.maxit
        eps = self.derivative_eps
        converged = []
        x_storage = []
        for i in range(maxit):
            x_storage.append(x0)
            gradf = grad_f(func,x0,eps)
            H = Hessian(func,x0,eps)
            try:
                dx = sp.solve(H,-gradf)
            except sp.LinAlgWarning:
                break
            x1 = x0 + dx
            converged = convergence(x0,x1,func,self.inf_rel_tol,
                                    self.inf_abs_tol,self.res_rel_tol,
                                    self.res_abs_tol)
            if converged[0]:
                x_storage.append(x1)
                end_time = time() - start_time
                break
            x0 = x1
        z = func(x_storage[-1])
        end_time = time() - start_time
        x_storage = np.array(x_storage)
        xmin = x_storage[:,0].min()
        ymin = x_storage[:,1].min()
        xmax = x_storage[:,0].max()
        ymax = x_storage[:,1].max()
        return x_storage, converged[0:5], i+1, z, 0, end_time, \
               [xmin,xmax,ymin,ymax]


class NT_LS_BT2(Solver):
    '''
    Solver child object
    Newton line search quadratic backtracking method
    '''
    def __init__(self):
        super().__init__()
        self.name = 'NT_LS_BT2'
        self.alpha = 0.0001
        self.maxit_inner = 20
        
    def solve(self,x0=None,func=None):
        '''
        Newton line search quadratic backtracking method solve function
        x0: initial guess
        func: a function to minimize 
        '''
        start_time = time()
        if self.func is not None:
            func = self.func
            if func is None:
                print('func is NOT defined')
                return
        if self.x0 is not None:
            x0 = self.x0
            if x0 is None:
                print('x0 is NOT defined')
                return
        maxit_inner = self.maxit_inner
        maxit = self.maxit
        eps = self.derivative_eps
        converged = []
        x_storage = []
        alpha = self.alpha
        inner_iter = 0
        for i in range(maxit):
            x_storage.append(x0)
            f = func(x0)
            F = grad_f(func,x0,eps)
            jac = Hessian(func,x0,eps)
            try:
                Y = sp.solve(jac,-F)
            except sp.LinAlgWarning:
                break
            initslope = np.dot(np.matmul(jac,Y),F)
    #        if initslope > 0.0:
    #            initslope = -initslope
    #        elif initslope == 0.0:
    #            initslope = -1.0
            lamda = 1.0
            W = x0 + lamda*Y
            g = func(W)
            x1 = W
            
            if g < f + alpha*lamda*initslope:
                pass
            else:
                for j in range(maxit_inner):
                    inner_iter += 1
                    # quadratic fit
                    lamdatemp = -initslope/2.0*(g-f-initslope) 
                    if lamdatemp > 0.5*lamda:
                        lamdatemp = 0.5*lamda
                    if lamdatemp <= 0.1*lamda:
                        lamda = 0.1*lamda
                    else:
                        lamda = lamdatemp
                    W = x0 + lamda*Y
                    g = func(W)
                    if g < f + lamda*alpha*initslope:
                        x1 = W
                        break
                if j == maxit_inner-1:
                    x1 = W
                    #print("maxit_inner")
                    #break
                    
            converged = convergence(x0,x1,func,self.inf_rel_tol,
                                    self.inf_abs_tol,self.res_rel_tol,
                                    self.res_abs_tol)
            if converged[0]:
                x_storage.append(x1)
                end_time = time() - start_time
                break
            x0 = x1
        z = func(x_storage[-1])
        end_time = time() - start_time
        x_storage = np.array(x_storage)
        xmin = x_storage[:,0].min()
        ymin = x_storage[:,1].min()
        xmax = x_storage[:,0].max()
        ymax = x_storage[:,1].max()
        return x_storage, converged[0:5], i+1, z, inner_iter, end_time, \
               [xmin,xmax,ymin,ymax]


class LS_BT1(Solver):
    '''
    Solver child object
    Steepest descent Line search linear backtracking method
    '''
    def __init__(self):
        super().__init__()
        self.name = 'LS_BT1'
        self.alpha = 1.0 
        self.c = 0.5
        self.tau = 0.5
        self.maxit_inner = 20
        
    def solve(self,x0=None,func=None):
        '''
        Line search linear backtracking method solve function
        x0: initial guess
        func: a function to minimize 
        '''
        start_time = time()
        if self.func is not None:
            func = self.func
            if func is None:
                print('func is NOT defined')
                return
        if self.x0 is not None:
            x0 = self.x0
            if x0 is None:
                print('x0 is NOT defined')
                return
        maxit_inner = self.maxit_inner
        maxit = self.maxit
        eps = self.derivative_eps
        alpha = self.alpha
        c = self.c
        tau = self.tau 
        converged = []
        x_storage = []
        inner_iter = 0
        for i in range(maxit):
            x_storage.append(x0)
            f = func(x0)
            gradf = grad_f(func,x0,eps)
            norm = sp.norm(gradf)
            unitgradf = gradf/norm
            initslope = np.dot(-gradf,unitgradf) #slope
            alpha = self.alpha
            x1 = x0-alpha*gradf
            g = func(x1)
            
            if g < f + alpha*c*initslope:
                pass
            else:
                for j in range(maxit_inner):
                    inner_iter += 1
                    alpha = tau*alpha
                    x1 = x0-alpha*gradf
                    g = func(x1)
                    if g < f + alpha*c*initslope:
                        break
                    
            converged = convergence(x0,x1,func,self.inf_rel_tol,
                                    self.inf_abs_tol,self.res_rel_tol,
                                    self.res_abs_tol)
            if converged[0]:
                x_storage.append(x1)
                end_time = time() - start_time
                break
            x0 = x1
            
        z = func(x_storage[-1])
        end_time = time() - start_time
        x_storage = np.array(x_storage)
        xmin = x_storage[:,0].min()
        ymin = x_storage[:,1].min()
        xmax = x_storage[:,0].max()
        ymax = x_storage[:,1].max()
        return x_storage, converged[0:5], i+1, z, inner_iter, end_time, \
               [xmin,xmax,ymin,ymax]


class LS_BT2(Solver):
    '''
    Solver child object
    Steepest descent Line search quadratic backtracking method
    '''
    def __init__(self):
        super().__init__()
        self.name = 'LS_BT2'
        self.alpha = 0.0001
        self.maxit_inner = 20
            
    def solve(self,x0=None,func=None):
        '''
        Line search quadratic backtracking method solve function
        x0: initial guess
        func: a function to minimize 
        '''
        start_time = time()
        if self.func is not None:
            func = self.func
            if func is None:
                print('func is NOT defined')
                return
        if self.x0 is not None:
            x0 = self.x0
            if x0 is None:
                print('x0 is NOT defined')
                return
        maxit_inner = self.maxit_inner
        maxit = self.maxit
        eps = self.derivative_eps
        alpha = self.alpha
        converged = []
        x_storage = []
        inner_iter = 0
        for i in range(maxit):
            x_storage.append(x0)
            f = func(x0)
            gradf = grad_f(func,x0,eps)
            norm = sp.norm(gradf)
            unitgradf = gradf/norm
            initslope = np.dot(-gradf,unitgradf)
            lamda = 1.0
            W = x0 - lamda*gradf
            g = func(W)
            x1 = W
            
            if g < f + lamda*alpha*initslope:
                pass
            else:
                for j in range(maxit_inner):
                    inner_iter += 1
                    # quadratic fit
                    lamdatemp = -initslope/2.0*(g-f-initslope) 
                    if lamdatemp > 0.5*lamda:
                        lamdatemp = 0.5*lamda
                    if lamdatemp <= 0.1*lamda:
                        lamda = 0.1*lamda
                    else:
                        lamda = lamdatemp
                    W = x0 - lamda*gradf
                    g = func(W)
                    if g < f + lamda*alpha*initslope:
                        x1 = W
                        break
                if j == maxit_inner-1:
                    x1 = W
                    #print("broke")
                    #break
                    
            converged = convergence(x0,x1,func,self.inf_rel_tol,
                                    self.inf_abs_tol,self.res_rel_tol,
                                    self.res_abs_tol)
            if converged[0]:
                x_storage.append(x1)
                end_time = time() - start_time
                break
            x0 = x1
        z = func(x_storage[-1])
        end_time = time() - start_time
        x_storage = np.array(x_storage)
        xmin = x_storage[:,0].min()
        ymin = x_storage[:,1].min()
        xmax = x_storage[:,0].max()
        ymax = x_storage[:,1].max()
        return x_storage, converged[0:5], i+1, z, inner_iter, end_time, \
               [xmin,xmax,ymin,ymax]
                    

class LS1_NT_HYB(Solver):
    '''
    Solver child object
    Steepest descent Line search linear and Newton hybrid method
    '''
    def __init__(self):
        super().__init__()
        self.name = 'LS1_NT_HYB'
        self.alpha = 1.0 
        self.c = 0.5
        self.tau = 0.5
        self.maxit_inner = 20
        
    def solve(self,x0=None,func=None):
        '''
        Line search linear and Newton hybrid method solve function
        x0: initial guess
        func: a function to minimize 
        '''
        start_time = time()
        if self.func is not None:
            func = self.func
            if func is None:
                print('func is NOT defined')
                return
        if self.x0 is not None:
            x0 = self.x0
            if x0 is None:
                print('x0 is NOT defined')
                return
        maxit_inner = self.maxit_inner
        maxit = self.maxit
        eps = self.derivative_eps
        alpha = self.alpha
        c = self.c
        tau = self.tau 
        converged = []
        x_storage = []
        inner_iter = 0
        for i in range(maxit):
            x_storage.append(x0)
            #Newton
            gradf = grad_f(func,x0,eps)
            H = Hessian(func,x0,eps)
            try:
                dx_nt = sp.solve(H,gradf)
            except sp.LinAlgWarning:
                break
            #linear Line Search
            norm = sp.norm(gradf)
            unitgradf = gradf/norm
            m = np.dot(-gradf,unitgradf)
            t = -c*m
            f0 = func(x0)
            for j in range(maxit_inner):
                res = f0-func(x0-alpha*gradf)
                if res >= alpha*t:
                    break
                else:
                    alpha = tau*alpha
                    inner_iter += 1
            dx_ls = alpha*gradf
            
            x_nt = x0 - dx_nt
            x_ls = x0 - dx_ls
            g_nt = func(x_nt)
            g_ls = func(x_ls)
            
            if g_nt < g_ls:
                x1 = x0 - dx_nt
            else:
                x1 = x0 - dx_ls
            alpha = 1.0
            
            converged = convergence(x0,x1,func,self.inf_rel_tol,
                                    self.inf_abs_tol,self.res_rel_tol,
                                    self.res_abs_tol)
            if converged[0]:
                x_storage.append(x1)
                end_time = time() - start_time
                break
            x0 = x1
      
        z = func(x_storage[-1])
        end_time = time() - start_time
        x_storage = np.array(x_storage)
        xmin = x_storage[:,0].min()
        ymin = x_storage[:,1].min()
        xmax = x_storage[:,0].max()
        ymax = x_storage[:,1].max()
        return x_storage, converged[0:5], i+1, z, inner_iter, end_time, \
               [xmin,xmax,ymin,ymax]


class LS2_NT_HYB(Solver):
    '''
    Solver child object
    Steepest descent Line search quadratic and Newton hybrid method
    '''
    def __init__(self):
        super().__init__()
        self.name = 'LS2_NT_HYB'
        self.alpha = 0.0001 
        self.maxit_inner = 10
        
    def solve(self,x0=None,func=None):
        '''
        Line search quadratic and Newton hybrid method solve function
        x0: initial guess
        func: a function to minimize 
        '''
        start_time = time()
        if self.func is not None:
            func = self.func
            if func is None:
                print('func is NOT defined')
                return
        if self.x0 is not None:
            x0 = self.x0
            if x0 is None:
                print('x0 is NOT defined')
                return
        maxit_inner = self.maxit_inner
        maxit = self.maxit
        eps = self.derivative_eps
        alpha = self.alpha
        converged = []
        x_storage = []
        inner_iter = 0
        for i in range(maxit):
            x_storage.append(x0)
            #Newton
            gradf = grad_f(func,x0,eps)
            H = Hessian(func,x0,eps)
            try:
                dx_nt = sp.solve(H,gradf)
            except sp.LinAlgWarning:
                break
            #linear Line Search
            f = func(x0)
            norm = sp.norm(gradf)
            unitgradf = gradf/norm
            initslope = np.dot(-gradf,unitgradf)
            lamda = 1.0
            x_ls = x0 - lamda*gradf
            g = func(x_ls)
            
            if g < f + lamda*alpha*initslope:
                pass
            else:
                for j in range(maxit_inner):
                    inner_iter += 1
                    # quadratic fit
                    lamdatemp = -initslope/2.0*(g-f-initslope) 
                    if lamdatemp > 0.5*lamda:
                        lamdatemp = 0.5*lamda
                    if lamdatemp <= 0.1*lamda:
                        lamda = 0.1*lamda
                    else:
                        lamda = lamdatemp
                    x_ls = x0 - lamda*gradf
                    g = func(x_ls)
                    if g < f + lamda*alpha*initslope:
                        break
       
            x_nt = x0 - dx_nt
            g_nt = func(x_nt)
            g_ls = func(x_ls)
            
            if g_nt < g_ls:
                x1 = x0 - dx_nt
            else:
                x1 = x_ls
            
            converged = convergence(x0,x1,func,self.inf_rel_tol,
                                    self.inf_abs_tol,self.res_rel_tol,
                                    self.res_abs_tol)
            if converged[0]:
                x_storage.append(x1)
                end_time = time() - start_time
                break
            x0 = x1
      
        z = func(x_storage[-1])
        end_time = time() - start_time
        x_storage = np.array(x_storage)
        xmin = x_storage[:,0].min()
        ymin = x_storage[:,1].min()
        xmax = x_storage[:,0].max()
        ymax = x_storage[:,1].max()
        return x_storage, converged[0:5], i+1, z, inner_iter, end_time, \
               [xmin,xmax,ymin,ymax]


class TR(Solver):
    '''
    Solver child object
    Trust region method with only Cauchy solution (no Newton)
    '''
    def __init__(self):
        super().__init__()
        self.name = 'TR'
        self.delta0 = 2.0
        self.eta1 = 0.2
        self.eta2 = 0.25
        self.eta3 = 0.75
        self.t1 = 0.25
        self.t2 = 2.0
        self.deltaM = 5.0
        self.method = 1  # 1:Cauchy, 2.Cauchy2, 3:Dogleg,
        
    def solve(self,x0=None,func=None):
        '''
        Trust region Cauchy method solve function (TRC)
        x0: initial guess
        func: a function to minimize 
        '''
        start_time = time()
        if self.func is not None:
            func = self.func
            if func is None:
                print('func is NOT defined')
                return
        if self.x0 is not None:
            x0 = self.x0
            if x0 is None:
                print('x0 is NOT defined')
                return
        maxit = self.maxit
        eps = self.derivative_eps
        delta0 = self.delta0
        eta1 = self.eta1
        eta2 = self.eta2 
        eta3 = self.eta3 
        t1 = self.t1 
        t2 = self.t2 
        deltaM = self.deltaM
        inner_it = False
        prev_p = x0
        x_storage = []
        converged = []
        delta_storage = []
        delta = delta0
        inner_iter = 0
        if (self.method == 1):
            method = cauchy_point
        elif (self.method == 2):
            method = cauchy_point2
        elif (self.method == 3):
            method = dogleg
        elif (self.method == 4):
            method = dogleg2
            
        for i in range(maxit):
            x_storage.append(x0)
            delta_storage.append(delta)
            if not inner_it:
                gradf = grad_f(func,x0,eps)
                H = Hessian(func,x0,eps)
            if sp.norm(gradf) == 0.0:
                end_time = time() - start_time                
                break
            if sp.norm(H) == 0.0:
                end_time = time() - start_time                
                break
            p = method(x0,gradf,H,delta,in_iter=inner_it)
            pTBp = np.dot(np.dot(p.transpose(),H),p)
            mp = func(x0) + np.dot(gradf.transpose(),p) + 0.5*pTBp
            f0 = func(x0)
            rho = (f0 - func(x0+p))/(f0-mp)
            if rho < eta2:
                delta_new = t1*delta
            else:
                if rho > eta3:
                    delta_new = min(t2*delta,deltaM)
                else:
                    delta_new = delta
            if rho > eta1:
                x1 = x0 + p
                inner_it = False
            else:
                x1 = x0
                inner_iter += 1
                inner_it = True
                if sp.norm(prev_p-p)/sp.norm(p) < 1.e-5:
                    break
            if sp.norm(x1-x0) > 1.e-50:
                converged = convergence(x0,x1,func,self.inf_rel_tol,
                                        self.inf_abs_tol,self.res_rel_tol,
                                        self.res_abs_tol)
                if converged[0]:
                    x_storage.append(x1)
                    delta_storage.append(delta)
                    end_time = time() - start_time
                    break
            prev_p = np.copy(p)
            delta = delta_new
            x0 = x1
        if converged == []:
            print("TR method failed without convergence.")
            return False
        outer_iter = i - inner_iter + 1
        z = func(x_storage[-1])
        end_time = time() - start_time
        x_storage = np.array(x_storage)
        xmin = x_storage[:,0].min()
        ymin = x_storage[:,1].min()
        xmax = x_storage[:,0].max()
        ymax = x_storage[:,1].max()
        return x_storage, converged[0:5], outer_iter, z, inner_iter, \
               end_time, [xmin,xmax,ymin,ymax], delta_storage


class LM(Solver):
    '''
    Solver child object
    Levenberg-Marquardt method
    '''
    def __init__(self):
        super().__init__()
        self.name = 'LM'
        self.mu = 0.25
        self.eta = 0.75
        self.delta = 0.0
        self.delta0 = 0.2
        self.delta1 = 0.3
        self.delta2 = 0.75
        self.delta3 = 2.0
        self.sigma = 0.0001
        
    def solve(self,x0=None,func=None):
        '''
        Levenberg-Marquardt method solve function
        x0: initial guess
        func: a function to minimize 
        '''
        start_time = time()
        if self.func is not None:
            func = self.func
            if func is None:
                print('func is NOT defined')
                return
        if self.x0 is not None:
            x0 = self.x0
            if x0 is None:
                print('x0 is NOT defined')
                return
        maxit = self.maxit
        eps = self.derivative_eps
        mu = self.mu
        eta = self.eta
        delta = self.delta 
        delta0 = self.delta0 
        delta1 = self.delta1
        delta2 = self.delta2
        delta3 = self.delta3
        sigma = self.sigma
        x_storage = []
        converged = []
        delta_storage = []
        xnorm = sp.norm(x0)
        delta = delta0*sp.norm(x0)
        inner_iter = 0
        for i in range(maxit):
            x_storage.append(x0)
            delta_storage.append(delta)
            # Newton
            gradf = grad_f(func,x0,eps)
            H = Hessian(func,x0,eps)
            try:
                Ytmp = sp.solve(H,gradf)
            except sp.LinAlgWarning:
                break
            fnorm = sp.norm(func(x0))
            nrm = sp.norm(Ytmp)
            norm1 = nrm
            while True:
                inner_iter += 1
                Y = Ytmp
                nrm = norm1
                if (nrm >= delta):
                    nrm = delta/nrm
                    gpnorm = (1.0-nrm)*fnorm
                    cnorm = nrm
                    Y = cnorm*Y
                    nrm = gpnorm
                else:
                    gpnorm = 0.0
                W = x0 - Y
                G = func(W)
                gnorm = sp.norm(G)
                if (fnorm == gpnorm):
                    rho = 0.0
                else:
                    rho = (fnorm**2.-gnorm**2.)/(fnorm**2.-gpnorm**2.)
                
                if (rho < mu):
                    delta *= delta1
                elif (rho < eta):
                    delta *= delta2
                else:
                    delta *= delta3
                converged = convergence(x0,W,func,self.inf_rel_tol,
                                        self.inf_abs_tol,self.res_rel_tol,
                                        self.res_abs_tol)
                if converged[0]:
                    x1 = W
                    #print("converged")
                    break
                if (rho > sigma):
                    x1 = W
                    break
                if (delta < xnorm*1.e-12):
                    #print("too small")
                    x1 = x0-Ytmp
                    break
            converged = convergence(x0,x1,func,self.inf_rel_tol,
                                    self.inf_abs_tol,self.res_rel_tol,
                                    self.res_abs_tol)
            if converged[0]:
                x_storage.append(x1)
                delta_storage.append(delta)
                end_time = time() - start_time
                break
            x0 = x1

        z = func(x_storage[-1])
        end_time = time() - start_time
        x_storage = np.array(x_storage)
        xmin = x_storage[:,0].min()
        ymin = x_storage[:,1].min()
        xmax = x_storage[:,0].max()
        ymax = x_storage[:,1].max()
        return x_storage, converged[0:5], i+1, z, inner_iter, \
               end_time, [xmin,xmax,ymin,ymax], delta_storage


class TR2(Solver):
    '''
    Solver child object
    Newton Trust region Dogleg Cauchy method (NTRDC)
    '''
    def __init__(self):
        super().__init__()
        self.name = 'TR'
        self.delta0 = 2.0
        self.eta1 = 0.2
        self.eta2 = 0.25
        self.eta3 = 0.75
        self.t1 = 0.25
        self.t2 = 2.0
        self.deltaM = 5.0

    def solve(self,x0=None,func=None):
        '''
        Trust region method solve function that uses
        both Newton and Cauchy solutions
        x0: initial guess
        func: a function to minimize 
        '''
        start_time = time()
        if self.func is not None:
            func = self.func
            if func is None:
                print('func is NOT defined')
                return
        if self.x0 is not None:
            x0 = self.x0
            if x0 is None:
                print('x0 is NOT defined')
                return
        maxit = self.maxit
        eps = self.derivative_eps
        delta0 = self.delta0
        eta1 = self.eta1
        eta2 = self.eta2 
        eta3 = self.eta3 
        t1 = self.t1 
        t2 = self.t2 
        deltaM = self.deltaM
        inner_it = False
        prev_p = x0
        x_storage = []
        converged = []
        delta_storage = []
        delta = delta0
        inner_iter = 0

        for i in range(maxit):
            x_storage.append(x0)
            delta_storage.append(delta)
            if not inner_it:
                gradf = grad_f(func,x0,eps)
                H = Hessian(func,x0,eps)
                gradf_norm = sp.norm(gradf)
                gTBg = np.dot(np.dot(gradf.transpose(),H),gradf)
                del_yqnk = sp.solve(H,gradf)
            if sp.norm(gradf) == 0.0:
                end_time = time() - start_time                
                break
            if sp.norm(H) == 0.0:
                end_time = time() - start_time                
                break

            if gTBg <= 0.0:
                auk = 1.0e9
            else:
                auk =  gradf_norm**2./gTBg
            ack = min(delta/gradf_norm, auk)
            del_yuk = ack*gradf    
            if sp.norm(del_yuk) >= delta:
                p = del_yuk
            else:
                if sp.norm(del_yqnk) <= delta:
                    p = del_yqnk
                else:
                    A = del_yuk
                    B = del_yqnk - del_yuk
                    c0 = sp.norm(B)**2.
                    c1 = 2.*np.dot(A,B)
                    c2 = sp.norm(A)**2. - delta**2.
                    tau_pos = (c1 + np.sqrt(c1**2. - 4.*c0*c2)) / (2.*c0)
                    tau_neg = (c1 - np.sqrt(c1**2. - 4.*c0*c2)) / (2.*c0)
                    tau = max([tau_pos, tau_neg])
                    p = del_yuk + tau*(del_yqnk - del_yuk)

            pTBp = np.dot(np.dot(p.transpose(),H),p)
            f0 = func(x0)
            mp = f0 - np.dot(gradf.transpose(),p) + 0.5*pTBp
            
            if f0 == mp:
                rho = 0.0
            else:
                rho = (f0 - func(x0-p))/(f0-mp)
            if rho < eta2:
                delta_new = t1*delta
            else:
                if rho > eta3:
                    delta_new = min(t2*delta,deltaM)
                else:
                    delta_new = delta
            if rho > eta1:
                x1 = x0 - p
                inner_it = False
            else:
                x1 = x0
                inner_iter += 1
                inner_it = True
                if sp.norm(prev_p-p)/sp.norm(p) < 1.e-5:
                    break
            if sp.norm(x1-x0) > 1.e-50:
                converged = convergence(x0,x1,func,self.inf_rel_tol,
                                        self.inf_abs_tol,self.res_rel_tol,
                                        self.res_abs_tol)
                if converged[0]:
                    x_storage.append(x1)
                    delta_storage.append(delta)
                    end_time = time() - start_time
                    break
            prev_p = np.copy(p)
            delta = delta_new
            x0 = x1
        if converged == []:
            print("TR method failed without convergence.")
            return False
        outer_iter = i - inner_iter + 1
        z = func(x_storage[-1])
        end_time = time() - start_time
        x_storage = np.array(x_storage)
        xmin = x_storage[:,0].min()
        ymin = x_storage[:,1].min()
        xmax = x_storage[:,0].max()
        ymax = x_storage[:,1].max()
        return x_storage, converged[0:5], outer_iter, z, inner_iter, \
               end_time, [xmin,xmax,ymin,ymax], delta_storage


class TR3(Solver):
    '''
    Solver child object
    Newton Trust region method (NTR)
    '''
    def __init__(self):
        super().__init__()
        self.name = 'TR'
        self.delta0 = 2.0
        self.eta1 = 0.2
        self.eta2 = 0.25
        self.eta3 = 0.75
        self.t1 = 0.25
        self.t2 = 2.0
        self.deltaM = 5.0

    def solve(self,x0=None,func=None):
        '''
        Trust region method solve function
        Only uses Newton solutions
        x0: initial guess
        func: a function to minimize 
        '''
        start_time = time()
        if self.func is not None:
            func = self.func
            if func is None:
                print('func is NOT defined')
                return
        if self.x0 is not None:
            x0 = self.x0
            if x0 is None:
                print('x0 is NOT defined')
                return
        maxit = self.maxit
        eps = self.derivative_eps
        delta0 = self.delta0
        eta1 = self.eta1
        eta2 = self.eta2 
        eta3 = self.eta3 
        t1 = self.t1 
        t2 = self.t2 
        deltaM = self.deltaM
        inner_it = False
        prev_p = x0
        x_storage = []
        converged = []
        delta_storage = []
        delta = delta0
        inner_iter = 0

        for i in range(maxit):
            x_storage.append(x0)
            delta_storage.append(delta)
            if not inner_it:
                gradf = grad_f(func,x0,eps)
                H = Hessian(func,x0,eps)
                gradf_norm = sp.norm(gradf)
                gTBg = np.dot(np.dot(gradf.transpose(),H),gradf)
                del_yqnk = sp.solve(H,gradf)
            if sp.norm(gradf) == 0.0:
                end_time = time() - start_time                
                break
            if sp.norm(H) == 0.0:
                end_time = time() - start_time                
                break

            if sp.norm(del_yqnk) <= delta:
                p = del_yqnk
            else:
                auk = delta/sp.norm(del_yqnk)
                p = del_yqnk*auk
            pTBp = np.dot(np.dot(p.transpose(),H),p)
            f0 = func(x0)
            mp = f0 - np.dot(gradf.transpose(),p) + 0.5*pTBp
            
            if f0 == mp:
                rho = 0.0
            else:
                rho = (f0 - func(x0-p))/(f0-mp)
            if rho < eta2:
                delta_new = t1*delta
            else:
                if rho > eta3:
                    delta_new = min(t2*delta,deltaM)
                else:
                    delta_new = delta
            if rho > eta1:
                x1 = x0 - p
                inner_it = False
            else:
                x1 = x0
                inner_iter += 1
                inner_it = True
                if sp.norm(prev_p-p)/sp.norm(p) < 1.e-5:
                    break
            if sp.norm(x1-x0) > 1.e-50:
                converged = convergence(x0,x1,func,self.inf_rel_tol,
                                        self.inf_abs_tol,self.res_rel_tol,
                                        self.res_abs_tol)
                if converged[0]:
                    x_storage.append(x1)
                    delta_storage.append(delta)
                    end_time = time() - start_time
                    break
            prev_p = np.copy(p)
            delta = delta_new
            x0 = x1
        if converged == []:
            print("TR method failed without convergence.")
            return False
        outer_iter = i - inner_iter + 1
        z = func(x_storage[-1])
        end_time = time() - start_time
        x_storage = np.array(x_storage)
        xmin = x_storage[:,0].min()
        ymin = x_storage[:,1].min()
        xmax = x_storage[:,0].max()
        ymax = x_storage[:,1].max()
        return x_storage, converged[0:5], outer_iter, z, inner_iter, \
               end_time, [xmin,xmax,ymin,ymax], delta_storage
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 13:43:58 2019

@author: heepark
"""

import numpy as np
import utility as u
import test_functions as tf
import nonlinear_solvers as ns
import test_engine as te

branin = tf.Branin()
matyas = tf.Matyas()
booth = tf.Booth()
boha = tf.Bohachevsky()
camel = tf.Threehumpcamel()
gold = tf.Goldstein()
sph = tf.Sphere()
rosen = tf.Rosenbrock()
levy = tf.Levy()
easom = tf.Easom()

sd = ns.SD()
nt = ns.NT()
nt_ls_bt2 = ns.NT_LS_BT2(); nt_ls_bt2.name='NT LSBT2'
ls_bt2 = ns.LS_BT2(); ls_bt2.name='SD LSBT2'
ls_bt1 = ns.LS_BT1()
ls1_nt_hyb = ns.LS1_NT_HYB()
ls2_nt_hyb = ns.LS2_NT_HYB()
trc1 = ns.TR(); trc1.method = 1; trc1.name='TRC'; # Cauchy point 1
trc2 = ns.TR(); trc2.method = 2; trc2.name='TRC2'; # Cauchy point 2
trd1 = ns.TR2(); trd1.method = 3; trd1.name='TRD';  # Dogleg
lm = ns.LM()
trc = ns.TR(); trc.method = 1; trc.name='TRC'; # Cauchy point 1
ntrdc = ns.TR2(); ntrdc.name='NTRDC';  # Dogleg
ntr = ns.TR3(); ntr.name='NTR'; # Cauchy point 1

sd.maxit = 250; sd.gamma = 0.1;

create_datapoints = False # False would load datapoints
if create_datapoints:
    te.sample_x0(branin.domain, 100, "branin")
    te.sample_x0(matyas.domain, 100, "matyas")
    te.sample_x0(booth.domain, 100, "booth")
    te.sample_x0(boha.domain, 100, "boha")
    te.sample_x0(np.array([[-1.,1],[1.0,2.5]]), 100, "camel")
    te.sample_x0(gold.domain, 100, "gold")
    te.sample_x0(sph.domain, 100, "sph")
    #te.sample_x0(rosen.domain, 100, "rosen")
    te.sample_x0(levy.domain, 100, "levy")
    te.sample_x0(easom.domain, 100, "easom")
else:
    branin_data = np.load("branin.npy")
    matyas_data = np.load("matyas.npy")
    booth_data = np.load("booth.npy")
    boha_data = np.load("boha.npy")
    camel_data = np.load("camel.npy")
    gold_data = np.load("gold.npy")
    sph_data = np.load("sph.npy")
    #rosen_data = np.load("rosen.npy")
    levy_data = np.load("levy.npy")
    easom_data = np.load("easom.npy")

plotit = 3
indiv_summary = True
methods = [nt, nt_ls_bt2, ls_bt2, ntrdc]
tests = []
#methods = [trd1]

trd1.derivative_eps = 1.e-4
nt.maxit = 8
Branin_test = te.TestEngine(function=branin,
                            methods=methods,
                            x0s=branin_data[0:100], contour_plot=plotit, 
                            plot_list=[24])
Branin_test.evaluate()
tests.append(Branin_test)
#Branin_test.print_report()
trd1.derivative_eps = 1.e-6
print("-----------------------------------------------------------------\n")

trd1.derivative_eps = 1.e-4
Matyas_test = te.TestEngine(function=matyas,
                            methods=methods,
                            x0s=matyas_data[0:100], contour_plot=plotit, 
                            plot_list=[0])
Matyas_test.evaluate()
tests.append(Matyas_test)
#Matyas_test.print_report()
trd1.derivative_eps = 1.e-6
print("-----------------------------------------------------------------\n")

nt.derivative_eps = 1.e-3
Booth_test = te.TestEngine(function=booth,
                           methods=methods,
                           x0s=booth_data[0:100], contour_plot=plotit, 
                           plot_list=[0])
Booth_test.evaluate()
tests.append(Booth_test)
#Booth_test.print_report()
nt.derivative_eps = 1.e-6
print("-----------------------------------------------------------------\n")

'''
nt.derivative_eps = 1.e-2
trd1.derivative_eps = 1.e-2
nt.maxit = 5
Boha_test = te.TestEngine(function=boha,
                          methods=methods,
                          x0s=boha_data[0:100], contour_plot=plotit, 
                          plot_list=[4])
Boha_test.sol_error_report = 1.0
Boha_test.evaluate()
tests.append(Boha_test)
#Boha_test.print_report()
nt.derivative_eps = 1.e-6
trd1.derivative_eps = 1.e-6
'''
print("-----------------------------------------------------------------\n")

trd1.derivative_eps = 1.e-5
nt.derivative_eps = 1.e-5
trd1.maxit = 10
nt.maxit = 4
Camel_test = te.TestEngine(function=camel,
                           methods=methods,
                           x0s=camel_data[np.r_[0,4,5,6,12,14,
                                                15,16,18,35]], 
                           contour_plot=plotit, 
                           plot_list=[0])
Camel_test.evaluate()
tests.append(Camel_test)
trd1.derivative_eps = 1.e-6
nt.derivative_eps = 1.e-6
trd1.maxit = 100
nt.maxit = 100
#Camel_test.print_report()
print("-----------------------------------------------------------------\n")


Gold_test = te.TestEngine(function=gold,
                          methods=methods,
                          x0s=gold_data[0:25], contour_plot=plotit, 
                          plot_list=[0])
Gold_test.evaluate()
tests.append(Gold_test)
#Gold_test.print_report()
print("-----------------------------------------------------------------\n")


trd1.derivative_eps = 7.e-4
nt.maxit = 3
Sph_test = te.TestEngine(function=sph,
                         methods=methods,
                         x0s=sph_data[0:50], contour_plot=plotit, 
                         plot_list=[0])
Sph_test.evaluate()
tests.append(Sph_test)
#Sph_test.print_report()
trd1.derivative_eps = 1.e-6
nt.maxit = 100
print("-----------------------------------------------------------------\n")

#trd1.derivative_eps = 7.e-4
#Rosen_test = te.TestEngine(function=rosen,
#                           methods=methods,
#                           x0s=rosen_data[0:15], contour_plot=plotit, 
#                           plot_list=[3])
#Rosen_test.evaluate()
#tests.append(Rosen_test)
#Rosen_test.print_report()
print("-----------------------------------------------------------------\n")


#Levy_test = te.TestEngine(function=levy,
#                          methods=methods,
#                          x0s=levy_data[0:100], contour_plot=plotit, 
#                          plot_list=[2])
#Levy_test.evaluate()
#tests.append(Levy_test)
#Levy_test.print_report()
print("-----------------------------------------------------------------\n")


Easom_test = te.TestEngine(function=easom,
                           methods=methods,
                           x0s=easom_data[0:10], contour_plot=plotit, 
                           plot_list=[0])
Easom_test.evaluate()
tests.append(Easom_test)
#Easom_test.print_report()
print("-----------------------------------------------------------------\n")

print("-----------------------------------------------------------")
te.print_summary(tests)

if indiv_summary:
    for test in tests:
        print(test.func.name)
        te.print_summary([test])
    
print("-----------------------------------------------------------\n")




